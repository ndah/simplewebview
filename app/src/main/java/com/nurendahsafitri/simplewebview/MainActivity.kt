package com.nurendahsafitri.simplewebview

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.Layout
import android.view.View
import android.webkit.*
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar

class MainActivity : AppCompatActivity() {

    private lateinit var mWebView: WebView
    private lateinit var progressBar: ProgressBar
    private lateinit var myCoordinatorLayout: CoordinatorLayout
    private lateinit var appConfigurationLayout: LinearLayout
    private lateinit var submitConfigurationButton: Button
    private lateinit var homepageConfiguration: EditText
    private lateinit var hostNameConfiguration: EditText
    private lateinit var sharedPref : SharedPreferences

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        mWebView = findViewById(R.id.activity_main_webView)
        progressBar = findViewById(R.id.progressBar_webView)
        myCoordinatorLayout = findViewById(R.id.myCoordinatorLayout)
        appConfigurationLayout = findViewById(R.id.appConfiguration)
        submitConfigurationButton = findViewById(R.id.submitConfigurationButton)
        homepageConfiguration = findViewById(R.id.homepage_text)
        hostNameConfiguration = findViewById(R.id.hostname_text)


        sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return

        submitConfigurationButton.setOnClickListener(MySubmitListener())

        if (!validConfiguration()) {
            mWebView.visibility = View.INVISIBLE
            appConfigurationLayout.visibility = View.VISIBLE
        }
        else {
            appConfigurationLayout.visibility = View.GONE
            loadWebView()
        }
    }

    private fun validConfiguration(): Boolean {
        val homepage = sharedPref.getString(getString(R.string.homepage_key), "")
        val hostname = sharedPref.getString(getString(R.string.hostname_key), "")
        return (homepage != "" && hostname != "")
    }

    override fun onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack()
        } else {
            showExitSnackBar()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun loadWebView() {

        var webSettings = mWebView.settings
        webSettings.javaScriptEnabled = true
        webSettings.setSupportMultipleWindows(false)

        webSettings.setAppCachePath(applicationContext.cacheDir.absolutePath)
        webSettings.allowFileAccess = true
        webSettings.setAppCacheEnabled(true)
        webSettings.cacheMode = WebSettings.LOAD_DEFAULT
        webSettings.builtInZoomControls = true;
        webSettings.displayZoomControls = false;

        val savedURL = sharedPref.getString(getString(R.string.homepage_key), "")
        mWebView.loadUrl(savedURL)

        // Stop local links and redirects from opening in browser instead of WebView
        mWebView.webViewClient = MyAppWebViewClient()
        (mWebView.webViewClient as MyAppWebViewClient).progressBar = progressBar
    }

    private fun super_onBackPressed(){
        super.onBackPressed()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun showRefreshSnackBar() {
        var mySnackBar = Snackbar.make(myCoordinatorLayout, R.string.ask_refresh_internet,
                Snackbar.LENGTH_INDEFINITE)
        mySnackBar.show()
        mySnackBar.setAction(R.string.refresh, MyRefreshListener())
    }

    private fun showExitSnackBar() {
        var mySnackBar = Snackbar.make(myCoordinatorLayout, R.string.ask_exit,
                Snackbar.LENGTH_SHORT)
        mySnackBar.show()
        mySnackBar.setAction(R.string.exit, MyExitListener())
    }

    inner class MyAppWebViewClient : WebViewClient() {

        private var timeout: Boolean = false
        lateinit var progressBar: ProgressBar

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest):Boolean {
            super.shouldOverrideUrlLoading(view, request)

            val savedHost = sharedPref.getString(getString(R.string.hostname_key), "")
            if(request.url.host.endsWith(savedHost)) {
                return false
            }

            var intent = Intent(Intent.ACTION_VIEW, request.url)
            view.context.startActivity(intent)
            return true
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)

            view?.visibility = View.VISIBLE
            this.timeout = false
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)

            view?.visibility = View.INVISIBLE
            this.timeout = false
            if(!isNetworkAvailable()){
                showRefreshSnackBar()
            }
            else{
                Thread(Runnable {
                    try {
                        Thread.sleep(6000)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }

                    if (this.timeout) {
                        showRefreshSnackBar()
                    }
                }).start()}
        }

        override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
            super.onReceivedError(view, request, error)

            view?.visibility = View.INVISIBLE
            showRefreshSnackBar()
        }
    }

    inner class MyRefreshListener : View.OnClickListener {
        override fun onClick(v: View) {
            mWebView.reload()
        }
    }

    inner class MyExitListener : View.OnClickListener {
        override fun onClick(v: View) {
            super_onBackPressed()
        }
    }

    inner class MySubmitListener : View.OnClickListener {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onClick(v: View) {

            with (sharedPref.edit()) {
                putString(getString(R.string.homepage_key), homepageConfiguration.text.toString())
                commit()
            }

            with (sharedPref.edit()) {
                putString(getString(R.string.hostname_key), hostNameConfiguration.text.toString())
                commit()
            }

            if (validConfiguration()) {
                appConfigurationLayout.visibility = View.GONE
                loadWebView()
            }
        }
    }
}
